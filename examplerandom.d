import rdlang.random, rdlang.matrix;
import std.stdio;

void main() {
	randInit(); // Have to initialize the RNG
	foreach(_; 0..5) { writeln(genInteger(1000)); }
	writeln("\n");
	foreach(_; 0..5) { writeln(genInteger(1000)); }
	writeln("---\n");
	writeln("Those were different. Let's set the seed.");
	setSeed(100);
	writeln("Run 1:");
	foreach(_; 0..5) { writeln(genInteger(1000)); }
	setSeed(100);
	writeln("\nRun 2:");
	foreach(_; 0..5) { writeln(genInteger(1000)); }
	
	writeln("\nGenerate a vector of draws from a U[10,20] distribution:");
	DoubleMatrix z = runif(10, 10.0, 20.0);
	z.print;
	writeln("\nGenerate a vector of draws from a N(0,5) distribution:");
	rnorm(10, 0.0, 5.0).print;
}