import rinsided, rdlang.matrix, rdlang.r, rdlang.reg, rdlang.vector;
import std.stdio;

void main() {
	evalRQ("y <- rnorm(100)");
	evalRQ("x <- cbind(1, matrix(rnorm(300), ncol=3))");
	evalRQ("fit <- lm(y ~ x - 1)");
	evalRQ("print(coefficients(fit))");
	auto y = RVector(evalR("y"));
	auto x = RMatrix(evalR("x"));
	OlsFit fit = lm(y, x);
	fit.print;
	evalRQ("print(residuals(fit))");
	fit.resids.print;
	writeln("SSE: ", fit.sse);
	
	// Use only the first 50 observations
	evalRQ("fit2 <- lm(y[1:50] ~ x[1:50,] - 1)");
	evalRQ("print(coefficients(fit2))");
	OlsFit fit2 = lmSubsample(y, x, 0, 49);
	fit2.coef.print;
}