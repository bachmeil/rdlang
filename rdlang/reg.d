
module rdlang.reg;

import rdlang.gretl, rdlang.matrix, rdlang.r, rdlang.vector;
import std.exception, std.stdio;

struct OlsFit {
  DoubleMatrix coef;
  DoubleMatrix vcv;
  DoubleMatrix resids;
  double s2;

  double sse() {
    double result = 0.0;
    foreach(e; resids.data) { 
      result += e*e; 
    }
    return result;
  }

  void print() {
    coef.print("Coefficients:");
    vcv.print("Coefficient covariance matrix:");
  }
}

OlsFit lm(GretlMatrix y, GretlMatrix x) {
  enforce(y.rows == x.rows, "y and x have different number of rows");
  enforce(y.cols == 1, "y can only have one column");
  auto coef = DoubleMatrix(x.cols);
  auto vcv = DoubleMatrix(x.cols, x.cols);
  auto resids = DoubleMatrix(y.rows);
  double s2;
  gretl_matrix_ols(y.matptr, x.matptr, coef.matptr, vcv.matptr, resids.matptr, &s2);
  return OlsFit(coef, vcv, resids, s2);
}

OlsFit lmSubsample(GretlMatrix y, GretlMatrix x, int r1, int r2) {
  return lm(y.copyRows(r1, r2+1), x.copyRows(r1, r2+1));
}
