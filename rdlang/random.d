
module rdlang.random;

import rdlang.gretl, rdlang.matrix, rdlang.vector, rdlang.r;
import std.exception;

void setSeed(uint seed) { 
  gretl_rand_set_seed(seed);
}

uint getSeed() { 
  return gretl_rand_get_seed();
}

void randInit() {
        gretl_rand_init();
}

bool usingBM() { 
  return gretl_rand_get_box_muller() > 0;
}

void useBM() { 
  gretl_rand_set_box_muller(1);
}

void useZiggurat() { 
  gretl_rand_set_box_muller(0);
}

uint genInteger(uint k) { 
  return gretl_rand_int_max(k);
}

DoubleMatrix runif(int n, double min=0.0, double max=1.0) {
  auto result = DoubleMatrix(n);
  gretl_rand_uniform_minmax(result.data.ptr, 0, n-1, min, max);
  return result;
}

double runif() { 
  return gretl_rand_01();
}

DoubleMatrix rnorm(int n, double mean=0.0, double sd=1.0) {
  auto result = DoubleMatrix(n);
  gretl_rand_normal_full(result.data.ptr, 0, n-1, mean, sd);
  return result;
}

double rnorm() { 
  return gretl_one_snormal(); 
}

DoubleMatrix rmvnorm(GretlMatrix mu, GretlMatrix V) {
  enforce(mu.cols == 1, "rmvnorm: mu needs to have one column");
  enforce(mu.rows == V.rows, "rmvnorm: mu and v need to have the same number of rows");
  enforce(V.rows == V.cols, "rmvnorm: v needs to be square");
  return mu + chol(V)*rnorm(mu.rows); // rnorm returns GretlVector, which aliases to a GretlMatrix
}
