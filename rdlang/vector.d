
module rdlang.vector;

import rdlang.gretl, rdlang.matrix, rdlang.r;
import std.conv, std.exception, std.stdio;

struct RVector {
  GretlMatrix mat;
  Robj robj;
  bool unprotect = true;

  this(int r) {
    Rf_protect(robj = Rf_allocVector(14,r));
    mat.rows = r;
    mat.cols = 1;
    mat.ptr = REAL(robj);
  }

  this(Robj rv) {
    enforce(isVector(rv), "In RVector constructor: Cannot convert non-vector R object to RVector");
    enforce(isNumeric(rv), "In RVector constructor: Cannot convert non-numeric R object to RVector");
    robj = rv;
    mat.rows = rv.length;
    mat.cols = 1;
    mat.ptr = REAL(rv);
    unprotect = false;
  }

  this(T)(T v) {
    Rf_protect(robj = Rf_allocVector(14,to!int(v.length)));
    mat.rows = to!int(v.length);
    mat.cols = 1;
    mat.ptr = REAL(robj);
    foreach(ii; 0..to!int(v.length)) {
      mat.ptr[ii] = v[ii];
    }
  }

  this(this) {
    unprotect = false;
  }

  ~this() {
    if (unprotect) {
      Rf_unprotect_ptr(robj); 
    }
  }

  double opIndex(int r) {
    enforce(r < mat.rows, "Index out of range: index on RVector is too large");
    return mat.ptr[r];
  }

  void opIndexAssign(double v, int r) {
    enforce(r < mat.rows, "Index out of range: index on RVector is too large");
    mat.ptr[r] = v;
  }

  void opAssign(T)(T x) {
    enforce(x.length == mat.rows, "Cannot assign to RVector from an object with the wrong length");
    foreach(ii; 0..to!int(x.length)) {
      this[ii] = x[ii];
    }
  }

  RVector opSlice(int i, int j) {
    enforce(j < mat.rows, "Index out of range: index on RVector slice is too large");
    enforce(i < j, "First index has to be less than second index");
    RVector result;
    result.mat.rows = j-i;
    result.mat.cols = 1;
    result.mat.ptr = &mat.ptr[i];
    result.mat.info = mat.info;
    result.robj = robj;
    result.unprotect = false;
    return result;
  }

  void print() {
    foreach(val; this) {
      writeln(val);
    }
  }

  bool empty() {
    return mat.rows == 0;
  }

  double front() {
    return this[0];
  }

  void popFront() {
    mat.ptr = &mat.ptr[1];
    mat.rows -= 1;
  }

  double[] array() {
    double[] result;
    result.reserve(mat.rows);
    foreach(val; this) {
      result ~= val;
    }
    return result;
  }

  alias mat this;
}

struct RIntVector {
  Robj robj;
  ulong length;
  int * ptr;
  bool unprotect = true;

  this(int r) {
    Rf_protect(robj = Rf_allocVector(13, r));
    length = r;
    ptr = INTEGER(robj);
  }

  this(int[] v) {
    Rf_protect(robj = Rf_allocVector(13, to!int(v.length)));
    length = to!int(v.length);
    ptr = INTEGER(robj);
    foreach(int ii, val; v) {
      this[ii] = val;
    }
  }

  this(Robj rv) {
    enforce(isVector(rv), "In RVector constructor: Cannot convert non-vector R object to RVector");
    enforce(isInteger(rv), "In RVector constructor: Cannot convert non-integer R object to RVector");
    robj = rv;
    length = rv.length;
    ptr = INTEGER(rv);
    unprotect = false;
  }

  this(this) {
    unprotect = false;
  }

  ~this() {
    if (unprotect) {
      Rf_unprotect_ptr(robj);
    }
  }

  int opIndex(int obs) {
    enforce(obs < length, "Index out of range: index on RIntVector is too large");
    return ptr[obs];
  }

  void opIndexAssign(int val, int obs) {
    enforce(obs < length, "Index out of range: index on RIntVector is too large");
    ptr[obs] = val;
  }

  void opAssign(int[] v) {
    foreach(int ii, val; v) {
      this[ii] = val;
    }
  }

  RIntVector opSlice(int i, int j) {
    enforce(j < length, "Index out of range: index on RIntVector slice is too large");
    enforce(i < j, "First index on RIntVector slice has to be less than the second index");
    RIntVector result;
    result.robj = this.robj;
    result.length = j-i;
    result.ptr = &this.ptr[i];
    result.unprotect = false;
    return result;
  }

  int[] array() {
    int[] result;
    result.reserve(length);
    foreach(val; this) {
      result ~= val;
    }
    return result;
  }

  void print() {
    foreach(val; this) {
      writeln(val);
    }
  }

  bool empty() {
    return length == 0;
  }

  int front() {
    return this[0];
  }

  void popFront() {
    ptr = &ptr[1];
    length -= 1;
  }
}
