
module rdlang.matrix;

import rdlang.r, rdlang.gretl;
import std.array, std.conv, std.exception, std.stdio, std.string;
import core.stdc.stdlib;

pragma(lib, "gretl-1.0");

private struct matrix_info {
  int t1;
  int t2;
  char **colnames;
  char **rownames;
}

struct GretlMatrix {
  int rows;
  int cols;
  double * ptr;
  matrix_info * info;

  GretlMatrix * matptr() {
    return &this;
  }

  double * val() {
    return ptr;
  }
  this(int r, int c) {
    GretlMatrix * m = gretl_matrix_alloc(r, c);
    rows = m.rows;
    cols = m.cols;
    ptr = m.ptr;
    info = m.info;
    core.stdc.stdlib.free(m);
  }

  void free() {
    core.stdc.stdlib.free(ptr);
    core.stdc.stdlib.free(info);
  }
  double opIndex(int r, int c) {
    return ptr[c*rows+r];
  }

  int[2] opSlice(int dim)(int begin, int end) {
    return [begin, end];
  }
  
  Submatrix opIndex(int[2] rr, int[2] cc) {
    return Submatrix(this, rr[0], cc[0], rr[1], cc[1]);
  }

  void opIndexAssign(double v, int r, int c) {
    ptr[c*rows+r] = v;
  }

  void opIndexAssign(double v, int[2] rr, int[2] cc) {
    auto sm = Submatrix(this, rr[0], cc[0], rr[1], cc[1]);
    sm = v;
  }
  void opAssign(GretlMatrix m) {
    enforce(this.rows == m.rows, "Number of rows is different");
    enforce(this.cols == m.cols, "Number of columns is different");
    foreach(ii; 0..rows*cols) {
      ptr[ii] = m.ptr[ii];
    }
  }

  void opAssign(double[] v) {
    enforce(this.rows*this.cols == to!int(v.length), "double[] has different number of elements from matrix");
    foreach(ii; 0..rows*cols) {
      ptr[ii] = v[ii];
    }
  }
  DoubleMatrix opBinary(string op)(double a) {
    static if(op == "+") {
      return matrixAddition(this, a);
    }
    static if(op == "-") {
      return matrixSubtraction(this, a);
    }
    static if(op == "*") {
      return matrixMultiplication(this, a);
    }
    static if(op == "/") {
      return matrixDivision(this, a);
    }
  }

  DoubleMatrix opBinaryRight(string op)(double a) {
    static if(op == "+") {
      return matrixAddition(this, a);
    }
    static if(op == "-") {
      return matrixSubtraction(a, this);
    }
    static if(op == "*") {
      return matrixMultiplication(this, a);
    }
    static if(op == "/") {
      return matrixDivision(a, this);
    }
  }

  DoubleMatrix opBinary(string op)(GretlMatrix m) {
    static if(op == "+") {
      return matrixAddition(this, m);
    }
    static if(op == "-") {
      return matrixSubtraction(this, m);
    }
    static if(op == "*") {
      return matrixMultiplication(this, m);
    }
  }
}

double mean(GretlMatrix gm) {
  enforce(gm.cols == 1, "mean requires a vector");
  return gretl_vector_mean(gm.matptr);
}

double var(GretlMatrix gm) {
  enforce(gm.cols == 1, "var requires a vector");
  return gretl_vector_variance(gm.matptr);
}

double sd(GretlMatrix gm) {
  enforce(gm.cols == 1, "sd requires a vector");
  return std.math.sqrt(gretl_vector_variance(gm.matptr));
}

DoubleMatrix dup(GretlMatrix m) {
  auto result = DoubleMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows*m.cols) {
    result.data[ii] = m.ptr[ii];
  }
  return result;
}

DoubleMatrix matrixAddition(GretlMatrix m, double a) {
  auto result = DoubleMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows*m.cols) {
    result.data[ii] = m.ptr[ii] + a;
  }
  return result;
}

DoubleMatrix matrixSubtraction(GretlMatrix m, double a) {
  auto result = DoubleMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows*m.cols) {
    result.data[ii] = m.ptr[ii] - a;
  }
  return result;
}

DoubleMatrix matrixSubtraction(double a, GretlMatrix m) {
  auto result = DoubleMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows*m.cols) {
    result.data[ii] = a - m.ptr[ii];
  }
  return result;
}

DoubleMatrix matrixMultiplication(GretlMatrix m, double a) {
  auto result = DoubleMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows*m.cols) {
    result.data[ii] = a*m.ptr[ii];
  }
  return result;
}

DoubleMatrix matrixDivision(GretlMatrix m, double a) {
  auto result = DoubleMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows*m.cols) {
    result.data[ii] = m.ptr[ii]/a;
  }
  return result;
}

DoubleMatrix matrixDivision(double a, GretlMatrix m) {
  auto result = DoubleMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows*m.cols) {
    result.data[ii] = a/m.ptr[ii];
  }
  return result;
}

DoubleMatrix matrixAddition(GretlMatrix x, GretlMatrix y) {
  enforce(x.rows == y.rows, "Differing number of rows in matrix addition");
  enforce(x.cols == y.cols, "Differing number of columns in matrix addition");
  auto result = DoubleMatrix(x.rows, x.cols);
  foreach(ii; 0..x.rows*x.cols) {
    result.data[ii] = x.ptr[ii] + y.ptr[ii];
  }
  return result;
}

DoubleMatrix matrixSubtraction(GretlMatrix x, GretlMatrix y) {
  enforce(x.rows == y.rows, "Differing number of rows in matrix subtraction");
  enforce(x.cols == y.cols, "Differing number of columns in matrix subtraction");
  auto result = DoubleMatrix(x.rows, x.cols);
  foreach(ii; 0..x.rows*x.cols) {
    result.data[ii] = x.ptr[ii] - y.ptr[ii];
  }
  return result;
}

DoubleMatrix matrixMultiplication(GretlMatrix x, GretlMatrix y) {
  enforce(x.cols == y.rows, "Dimensions do not allow for matrix multiplication");
  auto result = DoubleMatrix(x.rows, y.cols);
  gretl_matrix_multiply(x.matptr, y.matptr, result.matptr);
  return result;
}

void print(GretlMatrix m, string msg="") {
  gretl_matrix_print(&m, toStringz(msg));
}

DoubleMatrix copyRows(GretlMatrix m, int r0, int r1) {
  auto result = DoubleMatrix(r1-r0, m.cols);
  foreach(int row; r0..r1) {
    Row(result, row-r0) = Row(m, row);
  }
  return result;
}

struct DoubleMatrix {
  double[] data;
  int rows;
  int cols;
  private GretlMatrix temp;

  GretlMatrix mat() {
    temp.rows = this.rows;
    temp.cols = this.cols;
    temp.ptr = data.ptr;
    return temp;
  }

  GretlMatrix * matptr() {
    temp.rows = this.rows;
    temp.cols = this.cols;
    temp.ptr = data.ptr;
    return &temp;
  }

  double * ptr() {
    return data.ptr;
  }

  alias mat this;

  this(int r, int c=1) {
    data = new double[r*c];
    rows = r;
    cols = c;
  }
  this(RMatrix m) {
    data = new double[m.rows*m.cols];
    rows = m.rows;
    cols = m.cols;
    foreach(ii; 0..m.rows*m.cols) {
      data[ii] = m.ptr[ii];
    }
  }

  this(double[] v) {
    data = v;
    rows = to!int(v.length);
    cols = 1;
  }
  RMatrix opCast(T: RMatrix)() {
    auto result = RMatrix(rows, cols);
    result.mat = this.mat;
    return result;
  }

  double opIndex(int r, int c) {
    enforce(r < this.rows, "First index exceeds the number of rows");
    enforce(c < this.cols, "Second index exceed the number of columns");
    return data[c*this.rows + r];
  }

  void opIndexAssign(double v, int r, int c) {
    enforce(r < this.rows, "First index exceeds the number of rows");
    enforce(c < this.cols, "Second index exceed the number of columns");
    data[c*rows + r] = v;
  }

  int[2] opSlice(int dim)(int begin, int end) {
    return [begin, end];
  }
  
  Submatrix opIndex(int[2] rr, int[2] cc) {
    return Submatrix(this, rr[0], cc[0], rr[1], cc[1]);
  }

  void opIndexAssign(double v, int[2] rr, int[2] cc) {
    auto sm = Submatrix(this, rr[0], cc[0], rr[1], cc[1]);
    sm = v;
  }

  void opAssign(RMatrix m) {
    enforce(m.rows == this.rows, "Number of rows is different");
    enforce(m.cols == this.cols, "Number of columns is different");
    foreach(ii; 0..m.rows*m.cols) {
      data[ii] = m.ptr[ii];
    }
  }

  void opAssign(DoubleMatrix m) {
    enforce(m.rows == this.rows, "Number of rows is different");
    enforce(m.cols == this.cols, "Number of columns is different");
    data[] = m.data[];
  }

  void opAssign(double a) {
    data[] = a;
  }
}

void print(DoubleMatrix m, string msg="") {
  gretl_matrix_print(m.matptr, toStringz(msg));
}

struct RMatrix {
  Robj robj;
  bool unprotect = true;
  GretlMatrix mat;
  
  alias mat this;

  ~this() { 
    if (unprotect) { 
      Rf_unprotect_ptr(robj); 
    } 
  }
  this(int r, int c) {
    Rf_protect(robj = Rf_allocMatrix(14, r, c));
    mat.ptr = REAL(robj);
    mat.rows = r;
    mat.cols = c;
  }
  this(Robj rm) {
    enforce(isMatrix(rm), "Constructing RMatrix from something not a matrix"); 
    enforce(isNumeric(rm), "Constructing RMatrix from something that is not numeric");
    robj = rm;
    mat.ptr = REAL(rm);
    mat.rows = Rf_nrows(rm);
    mat.cols = Rf_ncols(rm);
    unprotect = false;
  }
  DoubleMatrix opCast(T: DoubleMatrix)() {
    auto result = DoubleMatrix(mat.rows, mat.cols);
    foreach(ii; 0..mat.cols) {
      Column(result, ii) = Column(mat, ii);
    }
    return result;
  }
  this(this) {
    unprotect = false;
  }

  int[2] opSlice(int dim)(int begin, int end) {
    return [begin, end];
  }
  
  Submatrix opIndex(int[2] rr, int[2] cc) {
    return Submatrix(this, rr[0], cc[0], rr[1], cc[1]);
  }

  Submatrix opIndex(int[2] rr, int cc) {
    return Submatrix(this, rr[0], cc, rr[1], cc+1);
  }

  Submatrix opIndex(int rr, int[2] cc) {
    return Submatrix(this, rr, cc[0], rr+1, cc[1]);
  }

  double opIndex(int r, int c) {
    enforce(r < this.rows, "First index exceeds the number of rows");
    enforce(c < this.cols, "Second index exceeds the number of columns");
    return mat.ptr[c*this.rows+r];
  }

  void opIndexAssign(double v, int[2] rr, int[2] cc) {
    auto sm = Submatrix(this, rr[0], cc[0], rr[1], cc[1]);
    sm = v;
  }

  void opIndexAssign(double v, int r, int c) {
    ptr[c*rows+r] = v;
  }

  void opIndexAssign(RMatrix m, int[2] rr, int[2] cc) {
    Submatrix temp = this[rr[0]..rr[1], cc[0]..cc[1]];
    temp = Submatrix(m);
  }

  void opAssign(double val) {
    ptr[0..this.rows*this.cols] = val;
  }

  GretlMatrix * matptr() {
    return &mat;
  }
}

RMatrix dup(RMatrix rm) { 
  RMatrix result;
  result.robj = Rf_protect(Rf_duplicate(rm.robj));
  result.ptr = REAL(result.robj);
  result.rows = rm.rows;
  result.cols = rm.cols;
  return result;
}

struct Row {
  GretlMatrix mat;
  int row;
  double * data;
  int length;

  this(GretlMatrix m, int r) {
    mat = m;
    row = r;
    data = m.ptr;
    length = m.cols;
  }
  double opIndex(int c) {
    enforce(c < length, "Row index out of bounds");
    return data[c*mat.rows+row];
  }
  void opIndexAssign(double val, int ii) {
    mat[row, ii] = val;
  }
  void opAssign(T)(T v) {
    enforce(this.length == v.length, "Attempting to copy into Row an object with the wrong number of elements");
    foreach(ii; 0..to!int(this.length)) {
      mat[row, ii] = v[ii];
    }
  }

  void opAssign(double x) {
    foreach(ii; 0..this.length) { 
      mat[row, ii] = x;
    }
  }
  bool empty() { return length == 0; }
  double front() { return data[0]; }
  void popFront() {
    data = &data[mat.rows];
    length -= 1;
  }
}

struct Col {
  GretlMatrix mat;
  int col;
  double * data;
  int length;

  this(GretlMatrix m, int c) {
    mat = m;
    col = c;
    data = m.ptr;
    length = m.rows;
  }

  double opIndex(int r) {
    enforce(r < length, "Column index out of bounds");
    return data[col*mat.rows+r];
  }

  void opIndexAssign(double val, int ii) {
    mat[ii, col] = val;
  }

  void opAssign(T)(T v) {
    enforce(this.length == v.length, "Attempting to copy into Column an object with the wrong number of elements");
    foreach(ii; 0..to!int(this.length)) {
      mat[ii, col] = v[ii];
    }
  }
 
  void opAssign(double x) {
    foreach(ii; 0..this.length) { 
      mat[ii, col] = x;
    }
  }

  void opAssign(GretlMatrix m) {
    enforce(length == m.rows, "Wrong number of elements to copy into Column");
    enforce(m.cols == 1, "Cannot copy into a Column from a matrix with more than one column");
    foreach(ii; 0..this.length) {
      mat[ii, col] = m[ii, 0];
    }
  }

  bool empty() { return length == 0; }
  double front() { return data[0]; }
  void popFront() {
    data = &data[1];
    length -= 1;
  }
}

void print(Row r) {
  foreach(val; r) {
    writeln(val);
  }
}

void print(Col c) {
  foreach(val; c) {
    writeln(val);
  }
}

struct ByRow {
  GretlMatrix mat;
  int rowno;
  int length;
  
  this(GretlMatrix m) {
    mat = m;
    rowno = 0;
    length = m.rows;
  }

  bool empty() { 
    return length == 0; 
  }
  
  Row front() { 
    return Row(mat, rowno); 
  }

  void popFront() {
    rowno += 1;
    length -= 1;
  }
}

struct ByColumn {
  GretlMatrix mat;
  int colno;
  int length;

  this(GretlMatrix m) {
    mat = m;
    colno = 0;
    length = m.cols;
  }

  bool empty() { 
    return length == 0; 
  }
  
  Col front() { 
    return Col(mat, colno); 
  }
  
  void popFront() {
    colno += 1;
    length -= 1;
  }
}

DoubleMatrix chol(GretlMatrix m) {
  enforce(m.rows == m.cols, "You are trying to compute a Cholesky decomposition of a non-square matrix");
  auto result = dup(m);
  int err = gretl_matrix_cholesky_decomp(result.matptr);
  enforce(err == 0, "Cholesky decomposition failed");
  return result;
}

DoubleMatrix inv(GretlMatrix m) {
  enforce(m.rows == m.cols, "You are trying to take the inverse of a non-square matrix");
  DoubleMatrix result = dup(m);
  int err = gretl_invert_matrix(result.matptr);
  enforce(err == 0, "Taking the inverse of a matrix failed");
  return result;
}

DoubleMatrix transpose(GretlMatrix m) {
  auto result = DoubleMatrix(m.cols, m.rows);
  int err = gretl_matrix_transpose(result.matptr, m.matptr);
  enforce(err == 0, "Taking the transpose of a matrix failed");
  return result;
}

double det(GretlMatrix m) {
  DoubleMatrix temp = dup(m);
  int err;
  double result = gretl_matrix_determinant(temp.matptr, &err);
  enforce(err == 0, "Taking the determinant of a matrix failed");
  return result;
}

double logdet(GretlMatrix m) {
  DoubleMatrix temp = dup(m);
  int err;
  double result = gretl_matrix_log_determinant(temp.matptr, &err);
  enforce(err == 0, "Taking the log determinant of a matrix failed");
  return result;
}

DoubleMatrix diag(GretlMatrix m) {
  enforce(m.rows == m.cols, "diag is not intended to be used to take the diagonal of a non-square matrix");
  auto result = DoubleMatrix(m.rows, 1);
  foreach(ii; 0..m.rows) { 
    result[ii,0] = m[ii,ii]; 
  }
  return result;
}

void setDiagonal(GretlMatrix m, GretlMatrix newdiag) {
  enforce(newdiag.cols == 1, "Cannot set a diagonal to a matrix with more than one column");
  enforce(m.rows == m.cols, "Cannot set the diagonal of a non-square matrix");
  enforce(m.rows == newdiag.rows, "Wrong number of elements in the new diagonal");
  foreach(ii; 0..newdiag.rows) {
    m[ii,ii] = newdiag[ii,0]; 
  }
}

void setDiagonal(T)(GretlMatrix m, T v) {
  enforce(m.rows == m.cols, "Cannot set the diagonal of a non-square matrix");
  enforce(m.rows == v.length, "Wrong number of elements in the new diagonal");
  foreach(ii; 0..m.rows) { 
    m[ii,ii] = v[ii]; 
  }
}

void setDiagonal(GretlMatrix m, double v) {
  enforce(m.rows == m.cols, "Attempting to set the diagonal of a non-square matrix");
  foreach(ii; 0..m.rows) { 
    m[ii,ii] = v; 
  }
}

double trace(GretlMatrix m) { 
  enforce(m.rows == m.cols, "Cannot take the trace of a non-square matrix");
  return gretl_matrix_trace(m.matptr); 
}

// Change this to copy into a DoubleMatrix

// DoubleMatrix pow(GretlMatrix m, int s) {
//   enforce(s >= 0, "Exponent on matrix has to be a positive integer. Use raise if you want element-by-element exponentiation.");
//   enforce(m.rows == m.cols, "Attempting to exponentiate a non-square matrix. Use raise if you want element-by-element exponentiation.");
//   int err;
//   auto temp = gretl_matrix_pow(m.matptr, s, &err);
//   enforce(err == 0, "Exponentiating a matrix failed");
//   return DoubleMatrix(temp);
// }

DoubleMatrix raise(GretlMatrix m, double k) {
  DoubleMatrix result = dup(m);
  gretl_matrix_raise(result.matptr, k);
  return result;
}

DoubleMatrix solve(GretlMatrix x, GretlMatrix y) {
  auto temp = dup(x);
  auto result = dup(y);
  int err = gretl_LU_solve(temp.matptr, result.matptr);
  enforce(err == 0, "Call to solve failed");
  return result;
}

DoubleMatrix eye(int k) {
  auto result = DoubleMatrix(k, k);
  result = 0.0;
  foreach(ii; 0..k) { 
    result[ii,ii] = 1.0;
  }
  return result;
}

DoubleMatrix kron(GretlMatrix x, GretlMatrix y) {
  auto result = DoubleMatrix(x.rows*y.rows, x.cols*y.cols);
  int err = gretl_matrix_kronecker_product(x.matptr, y.matptr, result.matptr);
  enforce(err == 0, "Kronecker product failed");
  return result;
}

private struct Observation {
  double value;
  int obs;
}

DoubleMatrix sort(bool inc=true)(GretlMatrix m, int col) {
  // Put all elements and row numbers in an array
  Observation[] temp;
  foreach(ii; 0..m.rows) { 
    temp ~= Observation(m[ii,col], ii); 
  }

  // Sort the array according to data
  static if (inc) {
    bool com(Observation x, Observation y) { return x.value < y.value; }
  } else {
    bool com(Observation x, Observation y) { return x.value > y.value; }
  }
  auto output = std.algorithm.sort!(com)(temp);

  // Now put the results into a new matrix
  auto result = DoubleMatrix(m.rows, m.cols);
  foreach(ii; 0..m.rows) { Row(result, ii) = Row(m, output[ii].obs); }
  return result;
}

int[] order(bool inc=true)(GretlMatrix m, int col) {
  Observation[] temp;
  foreach(ii; 0..m.rows) { 
    temp ~= Observation(m[ii,col], ii);
  }

  // Sort the array according to data
  static if (inc) {
    bool com(Observation x, Observation y) { return x.value < y.value; }
  } else {
    bool com(Observation x, Observation y) { return x.value > y.value; }
  }
  auto output = std.algorithm.sort!(com)(temp);
  int[] result;
  foreach(val; output) {
                result ~= val.obs;
        }
  return result;
}

DoubleMatrix stackColumns(GretlMatrix m) {
  auto result = DoubleMatrix(m.rows*m.cols, 1);
  foreach(ii; 0..result.rows) {
    result[ii,0] = m.ptr[ii];
  }
  return result;
}

DoubleMatrix stackRows(GretlMatrix m) {
  return stackColumns(m.transpose);
}

T cbind(T)(T[] mats) {
  int rows = mats[0].rows;
  int cols = 0;
  foreach(mat; mats) {
    enforce(mat.rows == rows, "Number of rows is not the same for all arguments");
    cols += mat.cols;
  }
  auto result = T(rows, cols);
  int startColumn = 0;
  foreach(mat; mats) {
    auto temp = result[0..rows, startColumn..startColumn+mat.cols];
    temp = Submatrix(mat);
    startColumn += mat.cols;
  }
  return result;
}

T rbind(T)(T[] mats) {
  int cols = mats[0].cols;
  int rows = 0;
  foreach(mat; mats) {
    enforce(mat.cols == cols, "Number of cols is not the same for all arguments");
    rows += mat.rows;
  }
  auto result = T(rows, cols);
  int startRow = 0;
  foreach(mat; mats) {
    result[startRow..startRow+mat.rows, 0..mat.cols] = mat;
    startRow += mat.rows;
  }
  return result;
}

struct Submatrix {
  // Original matrix
  double * ptr;
  int rows;

  // The submatrix
  int rowOffset;
  int colOffset;
  int subRows;
  int subCols;

  DoubleMatrix dup() {
    auto result = DoubleMatrix(subRows, subCols);
    foreach(col; 0..subCols) {
      foreach(row; 0..subRows) {
        result[row, col] = this[row, col];
      }
    }
    return result;
  }

  alias dup this;

  this(GretlMatrix m, int r0, int c0, int r1, int c1) {
    ptr = m.ptr;
    rows = m.rows;
    subRows = r1-r0;
    subCols = c1-c0;
    rowOffset = r0;
    colOffset = c0;
  }

  this(GretlMatrix m) {
    ptr = m.ptr;
    rows = m.rows;
    subRows = m.rows;
    subCols = m.cols;
    rowOffset = 0;
    colOffset = 0;
  }

  double opIndex(int r, int c) {
    enforce(r < subRows, "First index on Submatrix has to be less than the number of rows");
    enforce(c < subCols, "Second index on Submatrix has to be less than the number of columns");
    int newr = r+rowOffset;
    int newc = c+colOffset;
    return ptr[newc*rows + newr];
  }

  void opIndexAssign(double v, int r, int c) {
    enforce(r < subRows, "First index on Submatrix has to be less than the number of rows");
    enforce(c < subCols, "Second index on Submatrix has to be less than the number of columns");
    int newr = r+rowOffset;
    int newc = c+colOffset;
    ptr[newc*rows + newr] = v;
  }

  void opAssign(double v) {
    foreach(col; 0..subCols) {
      foreach(row; 0..subRows) {
        this[row, col] = v;
      }
    }
  }

  void opAssign(Submatrix m) {
    enforce(m.subRows == this.subRows, "Number of rows does not match");
    enforce(m.subCols == this.subCols, "Number of columns does not match");
    foreach(col; 0..subCols) {
      foreach(row; 0..subRows) {
        this[row, col] = m[row, col];
      }
    }
  }

  void opAssign(GretlMatrix m) {
    enforce(m.rows == this.subRows, "Number of rows does not match");
    enforce(m.cols == this.subCols, "Number of columns does not match");
    foreach(col; 0..m.cols) {
      foreach(row; 0..m.rows) {
        this[row, col] = m[row, col];
      }
    }
  }

  RMatrix rmat() {
    auto result = RMatrix(subRows, subCols);
    foreach(col; 0..subCols) {
      foreach(row; 0..subRows) {
        result[row, col] = this[row, col];
      }
    }
    return result;
  }   
}
