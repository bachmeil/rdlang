import rinsided, rdlang.matrix, rdlang.r, rdlang.vector;

RVector toRVector(string name) {
	return RVector(evalR(name));
}

void main() {
	evalRQ("plot(rnorm(100))");
	evalRQ("z <- rnorm(10)");
	RVector z = "z".toRVector;
	foreach (ii; 0..z.rows) {
		z[ii] = 2.5*z[ii];
	}
	z.toR("newz");
	evalRQ("plot(newz)");
}