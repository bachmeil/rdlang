// This file demonstrates the basics of interoperating with RInside.
// It consists of only three functions.
// Everything else is in rtod2.

import std.conv, std.stdio, std.string;
import rinsided, rdlang.r;

void main() {
	evalQuietlyInR(cast(char*)("x <- 3.5"));
	passToR(robj(12.6), cast(char*) "y");
	evalQuietlyInR(cast(char*) "z <- x*y");
	Robj rz = evalInR(cast(char*) "z");
	writeln("R returned the value ", rz.scalar);
}
