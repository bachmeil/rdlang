import rinsided, rdlang.r, rdlang.vector;
import std.stdio;

void main() {
	evalRQ("z <- list(a=rnorm(10), b=runif(25))");
	auto z = NamedList(evalR("z"));
	z.print;
	auto a = RVector(z["a"]);
	a.print;
}