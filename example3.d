// This file demonstrates the basics of interoperating with RInside.
// It consists of only three functions.
// Everything else is in rtod2.

import std.conv, std.stdio, std.string;
import rinsided, rdlang.r;

void main() {
	evalRQ("x <- 3.5");
	toR(12.6, "y");
	evalRQ("z <- x*y");
	Robj rz = evalR("z");
	writeln("R returned the value ", rz.scalar);
}
