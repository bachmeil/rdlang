module rinsided;
import std.string;
import rdlang.r;

pragma(lib, "R");

extern (C) {
	void passToR(Robj x, char * name);
	Robj evalInR(char * cmd);
	void evalQuietlyInR(char * cmd);
}

void toR(T)(T x, string name) {
	passToR(x.robj, (name.dup ~ '\0').ptr);
}

Robj evalR(string cmd) {
	char * v = (cmd.dup ~ '\0').ptr;
	return evalInR(v);
}

void evalRQ(string cmd) {
	evalQuietlyInR((cmd.dup ~ '\0').ptr);
}
