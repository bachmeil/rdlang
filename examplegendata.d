import rinsided, rdlang.matrix, rdlang.r, rdlang.random;
import std.exception, std.stdio;

RMatrix interactionDummy(RMatrix x, int starting) {
  enforce(starting < x.rows, "Starting index for generating the dummy variable has to be less than the number of rows");
  enforce(x.cols == 1, "interactionDummy currently supports only working with a single variable");
  auto result = RMatrix(x.rows, 1);
  result[0..starting,0..1] = 0.0;
  Submatrix temp = result[starting..x.rows,0..1];
  temp = x[starting..x.rows,0..1];
  return result;
}

RMatrix createRegressors(RMatrix x) {
  enforce(x.cols == 1, "createRegressors currently supports only working with a single variable");  
  auto result = RMatrix(x.rows, x.rows-1);
  Col(result, 0) = x;
  foreach(col; 1..result.cols) {
    Col(result, col) = interactionDummy(x, col);
  }
  return result;
}

void main() {
  writeln("this 0");
  auto m = RMatrix(5,1);
  writeln("this 1");
  Col(m, 0) = [1.1, 2.2, 3.3, 4.4, 5.5];
  m.print("M");
  RMatrix rhs = createRegressors(m);
  rhs.print("regressor matrix");
}