import rinsided, rdlang.optim, rdlang.r;
import std.stdio;

extern(C) {
  double f(int n, double * par, void * ex) {
    return par[0]*par[0] + par[1]*par[1];
  }

  void g(int n, double * par, double * gr, void * ex) {
    gr[0] = 2*par[0];
    gr[1] = 2*par[1];
  }
}

void main() {
  // This is a low-level example. If you really want to do so...
  double[] starting = [3.5, -5.5];
  double[] result = [1.0, 1.0];
  double currentValue;
  int fail = 1;
  int fncount = 100;
  nmmin(2, starting.ptr, result.ptr, &currentValue, &f, &fail, 0.000001, 0.000001, null, 1.0, 0.5, 2.0, 0, &fncount, 1000);
  writeln(result);
  writeln(fail);

  // Using the interfaces, which is what you are supposed to do...
  auto nm = NelderMead(&f);
  OptimSolution sol = nm.solve([3.5, -5.5]);
  writeln();
  sol.print;

  auto bfgs = BFGS(&f, &g);
  starting = [3.5, -5.5];
  OptimSolution sol2 = bfgs.solve(starting);
  writeln();
  sol2.print;
  
  auto cg = ConjugateGradient(&f, &g);
	starting = [3.5, -5.5];
	OptimSolution sol3 = cg.solve(starting);
	writeln();
	sol3.print;
	
	auto bounded = Bounded(&f, &g);
	starting = [3.5, -5.5];
	// Impose an upper bound on the first parameter, no bound on the second
	OptimSolution sol4 = bounded.solve(starting, [-10.0, -10.0], [-5.0, 10.0], [3, 0]);
	writeln();
	sol4.print;
	
	auto sa = SA(&f);
	starting = [3.5, -5.5];
	writeln("Starting sann");
 	OptimSolution sol5 = sa.solve(starting);
 	writeln("Finished with sann");
 	sol5.print;
}
