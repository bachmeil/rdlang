import std.exception, std.file, std.process, std.stdio, std.string;

string rdlangdir = "/opt/rdlang";
string[] names = ["gretl", "matrix", "r", "random", "reg", "vector", "optim"];
string modules;
string librinside = "/usr/local/lib/R/site-library/RInside/lib/libRInside.so";

void compileFile(string name) {
    string additional = "";
    if (exists("dmdadd.txt")) {
			additional = readText("dmdadd.txt");
		}
    auto cmd = "dmd " ~ name ~ ".d " ~ rdlangdir ~ "/rinsided.d " ~ modules ~ " -L" ~ rdlangdir ~ "/librinsided.so -L" ~ librinside ~ " " ~ additional;
    writeln(cmd);
    auto v = executeShell(cmd);
    writeln(v.output);
    v = executeShell("rm -f " ~ name ~ ".o");
    writeln(v.output);
}

void runFile(string name) {
    auto v = executeShell("LD_LIBRARY_PATH=" ~ rdlangdir ~ " ./" ~ name);
    writeln(v.output);
}

void main(string[] args) {
    foreach(name; names) {
        modules ~= rdlangdir ~ "/rdlang/" ~ name ~ ".d ";
    }
    enforce(args.length > 1, `

You didn't tell me which file to make.`);
    compileFile(args[1]);
    runFile(args[1]);
}
