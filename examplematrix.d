// Test the matrix functions
// Maybe testing can be automated in the future, but for now use your eyeballs.
// Testing isn't needed for the accuracy of the functions themselves
// because the hard work is done in well-tested C libraries
import rinsided, rdlang.gretl, rdlang.matrix, rdlang.r;
import std.stdio;

// Note: I use ` for strings so that all characters feed through to R. ` is a rarely used symbol in R. Can use " if you want.
void main() {
	evalRQ(`x <- matrix(c(2,3,6,8), ncol=2)`);
	Robj ry = evalR("x");
	auto y = RMatrix(ry);
	y.print("This matrix was created in R and passed to D");
	DoubleMatrix z = y*2.0;
	z.print("Doubling each element");
	writeln("Notice that we can mutate the values of an R matrix from inside D, so you have to be careful when doing this. Let's double one element of y and print out the matrix from within R.");
	y[0,0] = 657.238;
	evalRQ(`print(x)`);
	writeln("Matrix operations always create a new matrix, so operations like y*2.0 have no effect on y.");

	// Cholesky
	evalRQ(`x <- matrix(c(2,3,3,8), ncol=2)`);
	evalRQ(`print(chol(x))`);
	y = RMatrix(evalR("x"));
	y.chol.print;
	transpose(y.chol).print;

	// Inverse
	evalRQ(`print(solve(x))`);
	y.inv.print;

	// Transpose
	evalRQ(`m <- matrix(c(1.7,3.8,-1.9,4.5), ncol=2)`);
	evalRQ(`print(t(m))`);
	auto m = RMatrix(evalR("m"));
	m.transpose.print;

	// Determinant
	evalRQ(`print(det(m))`);
	writeln(m.det);

	// Diagonal - get and set
	evalRQ(`print(diag(x))`);
	y.diag.print;

	RMatrix m2 = m.dup;
	setDiagonal(m2, [3.9, -15.6]);
	m2.print;
	setDiagonal(m2, 3.5);
	m2.print;

	// Trace (R does not have this in the default installation)
	writeln(y.trace);

	// Element-by-element matrix exponentiation
	evalRQ(`print(m^1.7)`);
	print(m.raise(1.7));

	// Solve
	evalRQ(`print(solve(x, m))`);
	solve(y, m).print;

	// Create an identity matrix
	eye(3).print;

	// Kronecker product
	evalRQ(`print(kronecker(x, m))`);
	kron(y, m).print;

	// Sort a matrix
	evalRQ(`mm <- matrix(c(1.1, 2.2, 3.3, 5.7, 12.1, 1.9), ncol=2)`);
	auto mm = RMatrix(evalR("mm"));
	mm.print;
	print(mm.sort(1));
	writeln(mm.order(1));
	print(mm.sort!false(1)); // Decreasing
	writeln(mm.order!false(1));

	// Vectorizing
	print(mm);
	print(mm.stackColumns());
	print(mm.stackRows());

  auto zz = RMatrix(10, 10);
  zz[0,0] = 9.456;
  zz.print("zz");
  
  auto gm = GretlMatrix(5,4);
  foreach(ii; 0..20) {
    gm.ptr[ii] = 1.1*ii;
  }
  gm.print("gm");
  
  auto gsm = gm[1..3, 1..3];
  gsm.dup.print("Explicit dup");
  gsm.print("Alias this dup");
  gsm = 5.0;
  gm.print("modified gm");
  gm[3..5, 0..2] = 2.4;
  gm.print("further modified gm");
  gm.free();
  
  auto gv = GretlMatrix(3,1);
  gv = [1.2, 2.7, 3.1];
  writeln(gv.mean);
  writeln(gv.var);
  writeln(gv.sd);
}
