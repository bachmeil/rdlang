import std.exception, std.file, std.process, std.stdio;

// I'm sure this can be automated. I've already got the information, so this is easier for me.
string names = "rdlang/gretl rdlang/matrix rdlang/r rdlang/random rdlang/reg rdlang/vector rdlang/optim";
string librinside = "/usr/local/lib/R/site-library/RInside/lib/libRInside.so"; // Find this using find.package in R
string incrinside = "/usr/local/lib/R/site-library/RInside/include/"; // Header for RInside
string incrcpp = "/usr/local/lib/R/site-library/Rcpp/include/"; // Header for Rcpp
string incr = "/usr/share/R/include"; // Header for R API

void compileExample(string example) {
	auto v = executeShell("dmd example" ~ example ~ ".d rinsided.d " ~ names ~ " -L" ~ getcwd() ~ "/librinsided.so -L" ~ librinside);
	writeln(v.output);
	v = executeShell("rm -f example" ~ example ~ ".o");
	writeln(v.output);
}

void runExample(string example) {
	auto v = executeShell("LD_LIBRARY_PATH=" ~ getcwd() ~ " ./example" ~ example);
	writeln(v.output);
}

void main(string[] args) {
	enforce(args.length > 1, `

You didn't tell me what to make.

dmake cpplib to make the C++ interface
dmake foo to make example foo`);
	switch (args[1]) {
	case "cpplib":
		auto v = executeShell("g++ -c -fPIC librinsided.cpp -L" ~ librinside ~ " -I" ~ incrinside ~ " -I" ~ incrcpp ~ " -I" ~ incr);
		writeln(v.output);
		v = executeShell("gcc -shared -Wl,-soname,librinsided.so -o librinsided.so librinsided.o");
		writeln(v.output);
		break;
	default:
		compileExample(args[1]);
		runExample(args[1]);
		break;
	}
}
